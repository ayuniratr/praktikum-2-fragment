package com.ayunipuspaning_10191014.praktikum3fragmentbelajarfragment;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.ayunipuspaning_10191014.praktikum3fragmentbelajarfragment.adapter.TabFragmentPagerAdapter;

public class MainActivity extends AppCompatActivity {
    //deklarasi semua komponen View yang akan digunakan
    private Toolbar toolbar;
    private ViewPager pager;
    private TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set up toolbar
        toolbar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Material Tab");

            pager=(ViewPager)findViewById(R.id.pager);
            tabs=(TabLayout)findViewById(R.id.tabs);

            //set object com.ayunipuspaning_10191014.praktikum3fragmentbelajarfragment.adapter kedalam ViewPager
            pager.setAdapter(new TabFragmentPagerAdapter(getSupportFragmentManager()));

            //manipulasi sedikit untuk set TextColor pada Tab
            tabs.setTabTextColors(getResources().getColor(R.color.design_default_color_primary_dark),
                    getResources().getColor(android.R.color.white));

            //set tab ke ViewPager
            tabs.setupWithViewPager(pager);

            //konfigurasi Gravity Fill untuk Tab berada di posisi yang proporsional
            tabs.setTabGravity(TabLayout.GRAVITY_FILL);
    }
}